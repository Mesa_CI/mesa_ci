#!/usr/bin/env python3

from __future__ import print_function
import os
import signal
import sys
import time
from datetime import datetime

sys.path.append("/var/cache/mesa_jenkins/repos/mesa_ci")
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "../..", "build_support"))
from project_map import ProjectMap
from repo_set import Mirrors
from utils.utils import write_pid, signal_handler, signal_handler_quit


sys.argv[0] = os.path.abspath(sys.argv[0])


def main():
    # Write the PID file
    write_pid('/run/fetch_mirrors.pid')

    signal.signal(signal.SIGALRM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler_quit)
    signal.signal(signal.SIGTERM, signal_handler_quit)

    # running a service through intel's proxy requires some
    # annoying settings.
    os.environ["GIT_PYTHON_GIT_EXECUTABLE"] = "/usr/local/bin/git"
    # without this, git-remote-https spins at 100%
    os.environ["http_proxy"] = "http://proxy.jf.intel.com:911/"
    os.environ["https_proxy"] = "http://proxy.jf.intel.com:911/"
    cache_location = os.environ.get("FETCH_MIRRORS_CACHE_DIR")
    if not cache_location:
        cache_location = "/var/lib/git/mirror/"

    try:
        ProjectMap()
    except:
        sys.argv[0] = "/var/cache/mesa_jenkins/foo.py"

    if not os.path.exists(cache_location):
        os.makedirs(cache_location)
    # This *is* the service that creates the git cache, so do not use a cache
    # and create repos that are git clone mirrors:
    mirrors = Mirrors(path=cache_location)
    mirrors.poll()

if __name__ == "__main__":
    main()
