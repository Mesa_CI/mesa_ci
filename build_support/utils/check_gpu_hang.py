import socket
import subprocess

from export.export import fail_and_reboot, trigger_reboot
from project_map import ProjectMap

def report_hang(opts_obj,
                msg="",
                create_failing_test=False,
                reboot_system=False,
                kill_container="",
                raise_exception=False):
    print(msg)

    if create_failing_test:
        fail_and_reboot(f"gpu-hang-{socket.gethostname()}",
                        msg,
                        ProjectMap(opts=opts_obj))
    elif reboot_system:
        trigger_reboot(project_map=ProjectMap(opts=opts_obj))

    if kill_container:
        subprocess.call(["docker",
                         "kill",
                         kill_container])

    if raise_exception:
        raise RuntimeError("gpu hang detected")

def check_gpu_hang(opts_obj,
                   clear_coredump=False,
                   kill_container="",
                   create_failing_test=False,
                   reboot_system=False,
                   raise_exception=False,
                   watch_dog=False):

    xe_watch_paths = ["/sys/class/drm/card0/device/devcoredump/data",
                      "/sys/class/drm/card1/device/devcoredump/data"]

    i915_watch_paths = ["/sys/class/drm/card0/error",
                        "/sys/class/drm/card1/error"]
    if clear_coredump:
        for path in (xe_watch_paths + i915_watch_paths):
            try:
                with open(path, 'w') as fh:
                    fh.write("0")
            except (FileNotFoundError, PermissionError):
                print(f"INFO: No initial coredump found at {path}. "
                      "Skipping clear...")

    while True:
        for path in xe_watch_paths:
            try:
                with open(path) as fh:
                    report_hang(opts_obj,
                                msg=f"ERROR: gpu hang detected at {path}",
                                create_failing_test=create_failing_test,
                                reboot_system=reboot_system,
                                kill_container=kill_container,
                                raise_exception=raise_exception)
            except FileNotFoundError:
                continue

        for path in i915_watch_paths:
            try:
                with open(path, 'r') as fh:
                    if "No error state collected" in fh.read():
                        continue
                    report_hang(opts_obj,
                                msg=f"ERROR: gpu hang detected at {path}",
                                create_failing_test=create_failing_test,
                                reboot_system=reboot_system,
                                kill_container=kill_container,
                                raise_exception=raise_exception)
            except FileNotFoundError:
                continue
        if not watch_dog:
            return
