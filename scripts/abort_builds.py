import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))

from jenkins.jenkins import abort_builds

abort_builds()
