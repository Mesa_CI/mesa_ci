#!/usr/bin/env python3
import argparse
import datetime
import glob
import hashlib
import os
import subprocess
import sys
import tempfile
import time
import xml.etree.cElementTree as et
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from dependency_graph import DependencyGraph
from jenkins import Jenkins
from options import Options
from project_map import ProjectMap
from repo_set import RevisionSpecification
from utils.command import run_batch_command, rmtree
from export import convert_rsync_path, Export


def strip_passing_tests(xml):
    """ these statuses are interpreted as 'passing' CI expectations and will be
    stripped"""
    root = xml.getroot()
    strip_statuses = ['pass', 'skip', 'warn']
    if "disabled" in root.attrib:
        del root.attrib["disabled"]
    for a_suite in root.findall("testsuite"):
        if "disabled" in a_suite.attrib:
            del a_suite.attrib["disabled"]
        for a_test in a_suite.findall("testcase"):
            if a_test.findall("failure"):
                # never strip a failing test
                continue

            if a_test.findall("skipped"):
                # always strip a skipped test
                a_suite.remove(a_test)
                continue

            if "status" not in a_test.attrib:
                # tests without a status attribute are passing unless
                # they have a skip/failure tag.
                a_suite.remove(a_test)
                continue

            if a_test.attrib["status"] in strip_statuses:
                a_suite.remove(a_test)

        for a_test in a_suite.findall("testcase"):
            # jenkins junit cannot cope with the status attribute
            if "status" in a_test.attrib:
                del a_test.attrib["status"]

def collate_tests(result_path, out_test_dir, jenkins, strip_passes=False):
    """Aggregate the test files from the result path to the output
    directory, where results from other components will be collected."""
    if strip_passes:
        print("Note: passing tests will be stripped from the result file(s)")
        os.makedirs(f"{out_test_dir}/test/stripped", exist_ok=True)
    print(f"collecting tests from component subdirs in {result_path}")

    cmd = ["rsync", "-rlpD"]
    cmd += list(map(
        lambda invoke: convert_rsync_path(
            invoke.component_result_dir()
        ),
        jenkins.get_completed_builds()
    ))
    cmd += [out_test_dir]
    subprocess.call(cmd)

    # Junit files must have a recent time stamp or else Jenkins will
    # not parse them.
    for a_file in glob.glob(f"{out_test_dir}/test/**/*.xml", recursive=True):
        os.utime(a_file, None)

        # Remove passing tests from xml and write to different directory so
        # full results are preserved
        if strip_passes:
            a_file_name = a_file.split('/')[-1]
            a_new_file_path = f"{out_test_dir}/test/stripped/{a_file_name}"
            if os.path.isdir(a_file):
                continue
            try:
                xml = et.parse(a_file)
            except et.ParseError:
                continue
            strip_passing_tests(xml)
            xml.write(a_new_file_path, encoding='utf-8', xml_declaration=True)

def main():
    """build a project on Mesa's Jenkins instance"""
    # reuse the options from the gasket
    options = Options([sys.argv[0]])
    description="builds a component on jenkins"
    parser= argparse.ArgumentParser(description=description,
                                    parents=[options._parser],
                                    conflict_handler="resolve")
    parser.add_argument('--project', dest='project', type=str, default="",
                        help='Project to build. Default project is specified '\
                        'for the branch in build_specification.xml')

    parser.add_argument('--branch', type=str, default="mesa_master",
                        help="Branch specification to build.  "\
                        "See build_specification.xml/branches")

    parser.add_argument('--rebuild', type=str, default="false",
                        choices=['true', 'false'],
                        help="specific set of revisions to build."
                        "(default: %(default)s)")
    parser.add_argument('--results_subdir', type=str, default="",
                        help="Subdirectory under results_dir to place results."
                        " Use this to prevent conflicts when running"
                        "multiple concurrent tests on the same branch.")
    parser.add_argument('--env', type=str, default="", nargs="+",
                        help="Environment variables to pass to the build.")
    parser.add_argument('--internal', action='store_true',
                        help="Says whether we want to send test results to "
                        "only internal or both internal and external results sites")
    # TODO: remove this parameter should always be set to True.  When
    # we have switched fully to pipelines, it should be removed.  It
    # is not used anymore.
    parser.add_argument('--strip_passes', action='store_true',
                        help=("Strip passing tests from results. Results will "
                              "be placed under 'results/test/stripped'"))

    args = parser.parse_args()
    projects = []
    if args.project:
        projects = args.project.split(",")
    branch = args.branch
    rebuild = args.rebuild
    results_subdir = args.results_subdir or branch
    if isinstance(args.env, list):
        args.env = " ".join(args.env)
    internal = args.internal

    # some build_local params are not handled by the Options, which is
    # used by other modules.  This code strips out incompatible args
    options = Options(["bogus"])
    vdict = vars(args)
    del vdict["project"]
    del vdict["branch"]
    del vdict["rebuild"]
    del vdict["internal"]
    options.__dict__.update(vdict)
    sys.argv = ["bogus"] + options.to_list()

    project_map = ProjectMap()
    build_spec = project_map.build_spec()
    exporter = Export(project_map)
    rmtree(project_map.source_root() + "/test_summary.txt")

    if not projects:
        projects = build_spec.default_project_for_ci_branch(branch)
    dep_graph = DependencyGraph(projects, options)
    revspec = RevisionSpecification(only_projects=dep_graph.all_sources())
    priority = build_spec.priority_for_ci_branch(branch)

    print("Building revision: " + revspec.to_cmd_line_param())
    # create a result_path that is unique for this set of builds
    spec_xml = project_map.build_spec().xml()
    # perf CI results path should be results/mesa=<sha> and not a hash of
    # components.
    hostname = spec_xml.find("build_master").attrib.get('hostname')
    if hostname and hostname == 'otc-mesa-android':
        result_name = 'mesa=' + revspec.revision('mesa')
    else:
        result_name = hashlib.md5(revspec.to_cmd_line_param().encode('utf-8')).hexdigest()

    results_dir = spec_xml.find("build_master").attrib["results_dir"]
    result_path = "/".join([results_dir, results_subdir, result_name,
                            options.type])
    print(f"Result path: {result_path}")
    options.result_path = result_path
    # recreate the dependency graph with an options object that specifies the result path
    dep_graph = DependencyGraph(projects, options)

    if rebuild == "true" and exporter.result_path_exists(result_path):
        print("Removing existing results.")
        exporter.delete(result_path)

    # use a global, so signal handler can abort builds when scheduler
    # is interrupted
    global jen

    jen = Jenkins(result_path=result_path,
                  revspec=revspec,
                  internal=internal)

    out_test_dir = project_map.output_dir()
    if os.path.exists(out_test_dir):
        rmtree(out_test_dir)
    os.makedirs(out_test_dir)

    # Add a revisions.xml file
    with tempfile.TemporaryDirectory() as tempd:
        local_revs = f"{tempd}/revisions.xml"
        revspec.to_elementtree().write(local_revs)
        exporter.copy(local_revs, f"{result_path}/")

    # use a global, so signal handler can abort builds when scheduler
    # is interrupted
    try:
        jen.build_all(dep_graph, branch=branch, priority=priority)
    finally:
        print("INFO: proceeding to gather test results for jenkins...")
        collate_tests(
            result_path,
            out_test_dir,
            jen,
            strip_passes=True
        )

if __name__=="__main__":
    try:
        main()
    except SystemExit:
        print("Build failed")
        # Uncomment to determine which version of argparse is throwing
        # us under the bus.

        #  Word of Wisdom: Don't call sys.exit
        #import traceback
        #for x in traceback.format_exception(*sys.exc_info()):
        #    print(x)
        raise

# vim: ft=python
