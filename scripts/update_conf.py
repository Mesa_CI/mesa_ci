#!/usr/bin/env python3

import argparse
import datetime
from email.mime.text import MIMEText
import git
import os
import smtplib
import sys
import time
import importlib
import tempfile
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from bisect_test import retest_failures, TestLister
from export import convert_rsync_path, Export
from project_map import ProjectMap
from repo_set import RepoSet, RevisionSpecification
from utils.command import run_batch_command

parser = argparse.ArgumentParser(description="updates expected failures")

parser.add_argument('--blame_revision', type=str, required=True,
                    help='revision to specify as the cause of any config changes')
parser.add_argument('--result_path', metavar='result_path', type=str, default="",
                    help='path to build results')
parser.add_argument('--to', metavar='to', type=str, default="",
                    help='send resulting patch to this email')
parser.add_argument('--dir', metavar='dir', type=str, default="",
                    help='directory to bisect in')
args = parser.parse_args(sys.argv[1:])

# strip parameters to allow Options() to function
sys.argv = sys.argv[:1]

pm = ProjectMap()
exp = Export(pm)

# get revisions from out directory
_revspec = None
repos = RepoSet()
with tempfile.TemporaryDirectory() as tempd:
    local_revisions = os.path.join(tempd, "revisions.xml")

    exp.copy(os.path.join(args.result_path, "revisions.xml"), local_revisions)
    _revspec = RevisionSpecification.from_xml_file(local_revisions)

blame = args.blame_revision.rstrip().split("=")
if len(blame) != 2:
    print("ERROR: --blame_revision must be in the format: project=rev")
    sys.exit(-1)

if blame[0] not in repos.projects():
    print("ERROR: invalid project in --blame_revision: " + blame[0])
    print("ERROR: acceptable projects: " + ",".join(repos.projects()))
    sys.exit(-1)

spec_xml = pm.build_spec().xml()
results_dir = spec_xml.find("build_master").attrib["results_dir"]
retest_dir = args.dir
if retest_dir == "":
    retest_dir = results_dir + "/update/" + datetime.datetime.now().isoformat()

if _revspec.revision(blame[0]) != blame[1]:
    _revspec.set_revision(blame[0], blame[1])

# retest the set of failed tests on the specified blame revision
_revspec.checkout()

# mesa_ci may have changed
from bisect_test import retest_failures, TestLister
from export import convert_rsync_path
from project_map import ProjectMap
from repo_set import RepoSet, RevisionSpecification
from utils.command import run_batch_command

# use the full sha for the blame, so it can be looked up in a map when
# processing the config file
blame[1] = str(repos.repo(blame[0]).git.rev_parse(blame[1]))

if not retest_failures(args.result_path, retest_dir):
    print("ERROR: retest failed")
    sys.exit(-1)

reproduced_failures = TestLister(retest_dir + "/test/")

print("Found failures:")
reproduced_failures.Print()

for a_fail in reproduced_failures.Tests():
    a_fail.bisected_revision = " ".join(blame)
    a_fail.UpdateConf()

if args.to:
    patch_text = git.Repo().git.diff()
    print(patch_text)
    msg = MIMEText(patch_text)
    msg["Subject"] = "[PATCH] mesa jenkins updates due to " + args.blame_revision
    msg["From"] = "Do Not Reply <mesa_jenkins@intel.com>"
    msg["To"] = args.to
    s = smtplib.SMTP('or-out.intel.com')
    to = args.to.split(",")
    s.sendmail(msg["From"], to, msg.as_string())

    os.chdir(pm.source_root() + "/repos/mesa_ci_internal")
    r = git.Repo()
    patch_text = r.git.diff()
    if not patch_text:
        sys.exit(0)
    print(patch_text)
    msg = MIMEText(patch_text)
    msg["Subject"] = "[PATCH] internal config updates due to " + args.blame_revision
    msg["From"] = "Do Not Reply <mesa_jenkins@intel.com>"
    msg["To"] = args.to
    s = smtplib.SMTP('or-out.intel.com')
    to = args.to.split(",")
    s.sendmail(msg["From"], to, msg.as_string())
    r.git.reset("--hard")
