import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))

from utils import check_gpu_hang
from options import CustomOptions

if __name__ == "__main__":
    parser = CustomOptions(description="standalone CI gpu hang checker")

    parser.add_argument("--clear_coredump",
                        default=False,
                        action=argparse.BooleanOptionalAction,
                        help="NB: must run script as root")
    parser.add_argument("--create_failing_test",
                        default=False,
                        action=argparse.BooleanOptionalAction,
                        help="create a failing test in CI for this hang")
    parser.add_argument("--reboot_system",
                        default=False,
                        action=argparse.BooleanOptionalAction,
                        help="NB: implied by --create_failing_test")
    parser.add_argument("--raise_exception",
                        default=False,
                        action=argparse.BooleanOptionalAction,
                        help="raise a RuntimeError if a hang is found")
    parser.add_argument("--watch_dog",
                        default=False,
                        action=argparse.BooleanOptionalAction,
                        help="spin until a coredump file is found")
    parser.add_argument("--kill_container", nargs=1)

    args = parser.parse_args()

    check_gpu_hang.check_gpu_hang(parser,
                                  clear_coredump=args.clear_coredump,
                                  kill_container=args.kill_container,
                                  create_failing_test=args.create_failing_test,
                                  reboot_system=args.reboot_system,
                                  raise_exception=args.raise_exception,
                                  watch_dog=args.watch_dog)
